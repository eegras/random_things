#Minecraft Kerihunting

Datapack

Player Tracker: https://github.com/RedSparr0w/player_tracker/releases/tag/v1.3.0

Seed: `butts`

Commands:

First, locate the Stronghold.  This will be something like `-1024, ~, -1152`.  Anything else and something is wrong.
```
locate structure minecraft:stronghold
```

After you get the above, the below doesn't need changes.  If the coords above didn't match, there is a problem.
```
scoreboard objectives add KeriHealth health
setworldspawn -1024 71 -1152

execute in minecraft:overworld run worldborder center -1024 -1152
execute in minecraft:overworld run worldborder 512

execute in minecraft:the_nether run worldborder center -128 -144
execute in minecraft:the_nether run worldborder 512

execute in minecraft:the_end run worldborder center 0 0
execute in minecraft:the_end run worldborder 512

scoreboard objectives add Deaths deathCount
scoreboard objectives setdisplay list Deaths

team add Prey
team modify Prey color pink
team add Prey TheSmolBear
execute as @p[team=Prey] unless entity @s[tag=old] run tag @s add old

team add Hunters
team modify Hunters color blue

bossbar add bosskeri "Keri"
bossbar set minecraft:bosskeri color pink
bossbar set minecraft:bosskeri max 20

setblock -1026 -64 -1149 repeating_command_block[conditional=false,facing=up] replace
setblock -1025 -64 -1149 repeating_command_block[conditional=false,facing=up] replace
setblock -1024 -64 -1149 repeating_command_block[conditional=false,facing=up] replace
setblock -1023 -64 -1149 repeating_command_block[conditional=false,facing=up] replace
setblock -1022 -64 -1149 repeating_command_block[conditional=false,facing=up] replace

data merge block -1026 -64 -1149 {Command:"execute as @p unless entity @s[tag=old] run effect give @s minecraft:resistance 30 100",auto:1b}
data merge block -1025 -64 -1149 {Command:"execute as @p unless entity @s[tag=old] run team join Hunters",auto:1b}
data merge block -1024 -64 -1149 {Command:"execute as @p unless entity @s[tag=old] run tag @s add old",auto:1b}
data merge block -1023 -64 -1149 {Command:"execute as @a store result bossbar minecraft:bosskeri value run scoreboard players get TheSmolBear KeriHealth",auto:1b}
data merge block -1022 -64 -1149 {Command:"bossbar set minecraft:bosskeri players @a[team=Hunters]",auto:1b}
```
