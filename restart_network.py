import requests
from subprocess import DEVNULL, STDOUT, check_call, CalledProcessError
import time


actually_restart = False

hostname_to_ping = '192.168.101.1'  # If this is down for more than 5 tests, reboot the modem.
modem_ip_address = '192.168.100.1'
modem_username = 'admin'
modem_password = 'password'
not_up_threshold = 5

pushover_apikey = ''
pushover_userkey = ''


def isUp (hostname):
    try:
        response = check_call("ping -n 1 {}".format(hostname), stdout=DEVNULL, stderr=STDOUT)
    except CalledProcessError:
        response = 1

    if response == 0:
        print("{} is up.".format(hostname))
        return True
    print("{} is down.".format(hostname))
    return False


def nth(number=0):
    last_int = number % 10
    if last_int == 1:
        return 'st'
    if last_int == 2:
        return 'nd'
    if last_int == 3:
        return 'rd'
    return 'th'


def restartModem(ip_address, username, password):
    global actually_restart
    payload = {'loginUsername': username,
               'loginPassword': password,
               'resetbt': '1'}

    url = 'http://{}/goform/home_loggedout'.format(ip_address)
    page = requests.post(url, payload)
    if "<li><a href='home_loggedout.asp'>logout</a></li>" in page.text.lower():
        print("Logged in successfully.")
        url = 'http://{}/goform/restore_reboot'.format(ip_address)
        if actually_restart:
            page = requests.post(url, payload)
            if 'the device is being reset' in page.text.lower():
                print("Restarted modem.")
                return True
            print("Didn't restart modem.")
            print(page.text)
            return False
        else:
            print('Would have rebooted modem here.')
            return True
    else:
        print("Could not log in.")
        return False


def sendNotification(title, message):
    global pushover_apikey
    global pushover_userkey
    payload = dict()
    payload['token'] = pushover_apikey
    payload['user'] = pushover_userkey
    payload['title'] = title
    payload['message'] = message

    try:
        requests.post('https://api.pushover.net/1/messages.json', payload)
    except:
        print("Could not send message.")


def checkDownNTimes(ip_address, max_times):
    times = 0
    while True:
        if isUp(ip_address):
            times = 0
        else:
            times += 1

        if times >= max_times:
            return True
        time.sleep(1)


def checkUpNTimes(ip_address, max_times):
    times = 0
    while True:
        if not isUp(ip_address):
            times = 0
        else:
            times += 1

        if times >= max_times:
            return True
        time.sleep(1)


times_not_up = 0

while True:
    if checkDownNTimes(hostname_to_ping, not_up_threshold):
        sendNotification("Restarting modem", "Lost connection to Google {} times in a row.".format(times_not_up))
        print("Restarting modem at {}".format(modem_ip_address))

        restartModem(modem_ip_address, modem_username, modem_password)
        print("Waiting for modem to go down...")

        checkDownNTimes(modem_ip_address, 5)
        print("Modem shutdown.  Waiting for it to come back online.")

        checkUpNTimes(modem_ip_address, 5)
        sendNotification("Modem restarted", "Confirmed modem restart.")

        times_not_up = 0
    time.sleep(1)
